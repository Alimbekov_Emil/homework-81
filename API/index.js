const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const { nanoid } = require("nanoid");
const exitHook = require("async-exit-hook");
const Link = require("./models/Link");

const app = express();
app.use(express.json());
app.use(cors());

const port = 8000;

app.get("/:shortUrl", async (req, res) => {
  try {
    const result = await Link.findOne({ shortUrl: req.params.shortUrl });
    if (result) {
      res.status(301).redirect(result.originalUrl);
    } else {
      res.sendStatus(404);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

app.post("/links", async (req, res) => {
  try {
    const linkData = req.body;
    const link = new Link({ ...linkData, shortUrl: nanoid(10), _id: nanoid(6) });

    await link.save();

    res.send(link);
  } catch (e) {
    res.sendStatus(500);
  }
});

const run = async () => {
  await mongoose.connect("mongodb://localhost/link-shortener", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  exitHook(async (callback) => {
    await mongoose.disconnect();
    callback();
  });
};

run().catch(console.error);
