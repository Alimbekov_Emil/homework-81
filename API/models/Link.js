const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      required: true,
    },
    shortUrl: {
      type: String,
      required: true,
    },
    originalUrl: {
      type: String,
      required: true,
    },
  },
  {
    versionKey: false,
  }
);

const Link = mongoose.model("Link", ProductSchema);

module.exports = Link;
