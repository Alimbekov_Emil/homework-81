import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./App.css";
import { postOriginalUrl } from "./store/actions";
import Spinner from "./Components/Spinner/Spinner";

const App = () => {
  const dispatch = useDispatch();
  const [originalUrl, setOriginalUrl] = useState("");

  const links = useSelector((state) => state.links);
  const loading = useSelector((state) => state.loading);

  const changeLink = (e) => {
    setOriginalUrl(e.target.value);
  };

  const submitForm = (e) => {
    e.preventDefault();

    dispatch(postOriginalUrl(originalUrl));
  };

  return (
    <div className="App">
      <h1>Shorten Your Link!</h1>
      <form onSubmit={submitForm}>
        <input type="text" placeholder="Enter URL here" value={originalUrl} onChange={changeLink} />
        <button>Shorten</button>
        {loading ? <Spinner /> : null}
      </form>
      {links !== false ? (
        <div>
          <p>Your Link now looks like this:</p>
          <a href={links.originalUrl} target="_blank" rel="noreferrer">
            {"http//:localhost:8000/" + links.shortUrl}
          </a>
        </div>
      ) : null}
    </div>
  );
};

export default App;
