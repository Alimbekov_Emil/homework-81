import axiosLink from "../axiosLink";

export const POST_ORIGINAL_URL_REQUEST = "POST_ORIGINAL_URL_REQUEST";
export const POST_ORIGINAL_URL_SUCCESS = "POST_ORIGINAL_URL_SUCCESS";
export const POST_ORIGINAL_URL_FAILURE = "POST_ORIGINAL_URL_FAILURE";

export const postOriginalUrlRequest = () => ({ type: POST_ORIGINAL_URL_REQUEST });
export const postOriginalUrlSuccess = (links) => ({ type: POST_ORIGINAL_URL_SUCCESS, links });
export const postOriginalUrlFailure = (error) => ({ type: POST_ORIGINAL_URL_FAILURE, error });

export const postOriginalUrl = (url) => {
  return async (dispatch) => {
    try {
      dispatch(postOriginalUrlRequest());
      const response = await axiosLink.post("/links", { originalUrl: url });
      dispatch(postOriginalUrlSuccess(response.data));
    } catch (e) {
      dispatch(postOriginalUrlFailure(e));
    }
  };
};
