import { POST_ORIGINAL_URL_FAILURE, POST_ORIGINAL_URL_REQUEST, POST_ORIGINAL_URL_SUCCESS } from "./actions";

const initialState = {
  error: false,
  links: false,
  loading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_ORIGINAL_URL_REQUEST:
      return { ...state, loading: true };
    case POST_ORIGINAL_URL_SUCCESS:
      return { ...state, loading: false, links: action.links };
    case POST_ORIGINAL_URL_FAILURE:
      return { ...state, loading: false, error: action.error };
    default:
      return state;
  }
};

export default reducer;
